
PREFIX=arm-none-eabi-
AS=$(PREFIX)gcc -x assembler-with-cpp
CC=$(PREFIX)gcc
LD=$(PREFIX)ld

OBJDUMP=$(PREFIX)objdump

CPU = -mcpu=cortex-m7
FPU = -mfpu=fpv5-d16
FLOAT-ABI = -mfloat-abi=hard
MCU = $(CPU) -mthumb $(FPU) $(FLOAT-ABI)

obj = \
						stm32/stm32h7xx_hal_cortex.o \
						stm32/stm32h7xx_hal_rcc.o \
						stm32/stm32h7xx_hal_rcc_ex.o \
						stm32/stm32h7xx_hal_flash.o \
						stm32/stm32h7xx_hal_flash_ex.o \
						stm32/stm32h7xx_hal_gpio.o \
						stm32/stm32h7xx_hal_dma.o \
						stm32/stm32h7xx_hal_dma_ex.o \
						stm32/stm32h7xx_hal_pwr.o \
						stm32/stm32h7xx_hal_pwr_ex.o \
						stm32/stm32h7xx_hal.o \
						stm32/stm32h7xx_hal_exti.o \
						stm32/stm32h7xx_hal_tim.o \
						stm32/stm32h7xx_hal_tim_ex.o \
						stm32/stm32h7xx_hal_uart.o \
						stm32/stm32h7xx_hal_uart_ex.o \
						stm32/startup_stm32h743xx.o \
						stm32/system_stm32h7xx.o \
						init/stm32h7xx_it.o \
						init/stm32h7xx_hal_msp.o \
						init/system_init.o \
						io/io.o \
						cmd/cmd.o \
						init/main.o \
						aamem/aamem.o \
						xmodem/xmodem.o

ldscript = STM32H743ZITx_FLASH.ld

ASFLAGS=$(MCU)

CFLAGS=$(MCU)
CFLAGS+=-I. \
								-Icmsis \
								-Istm32 \
								-Iinit \
								-Iio \
								-Idelay \
								-Icmd \
								-Iaamem \
								-Ixmodem

CFLAGS+=-DSTM32H743xx
# -DUSE_HAL_DRIVER

LDFLAGS=$(MCU)
LDFLAGS+=--specs=nosys.specs

all : prog.elf

%.o : %.s
	@echo [AS] $@
	@$(CC) -c -g $(ASFLAGS) \
		-o $@ $<

%.o : %.c
	@echo [CC] $@
	@$(CC) -c -g $(CFLAGS) \
		-o $@ $<

prog.elf : $(obj)
	@echo [LD] $@
	@$(CC) $(LDFLAGS) \
		-T $(ldscript) \
		-o $@ $^

clean :
	rm -rf $(obj) prog.elf

objdump : prog.elf
	$(OBJDUMP) -d prog.elf

flash : prog.elf
	openocd -f board/st_nucleo_h743zi.cfg \
		-c "init" -c "program $< verify reset" \
		-c 'shutdown'
