#include "system_init.h"
#include "stm32h7xx_hal.h"
#include <string.h>

UART_HandleTypeDef UartHandle;

void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART3_UART_Init(void);

int system_init(void){
 HAL_Init();
 SystemClock_Config();
 MX_GPIO_Init();
 MX_USART3_UART_Init();
}

void SystemClock_Config(void){
 RCC_OscInitTypeDef RCC_OscInitStruct = {0};
 RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
 HAL_PWREx_ConfigSupply(PWR_LDO_SUPPLY);
 __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);
 while(!__HAL_PWR_GET_FLAG(PWR_FLAG_VOSRDY))
  ;
 __HAL_RCC_PLL_PLLSOURCE_CONFIG(RCC_PLLSOURCE_HSE);
 RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
 RCC_OscInitStruct.HSEState = RCC_HSE_BYPASS;
 RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
 RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
 RCC_OscInitStruct.PLL.PLLM = 1;
 RCC_OscInitStruct.PLL.PLLN = 120;
 RCC_OscInitStruct.PLL.PLLP = 4;
 RCC_OscInitStruct.PLL.PLLQ = 4;
 RCC_OscInitStruct.PLL.PLLR = 2;
 RCC_OscInitStruct.PLL.PLLRGE = RCC_PLL1VCIRANGE_3;
 RCC_OscInitStruct.PLL.PLLVCOSEL = RCC_PLL1VCOWIDE;
 RCC_OscInitStruct.PLL.PLLFRACN = 0;
 if(HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK){
  Error_Handler();
 }
 RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
  |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2
  |RCC_CLOCKTYPE_D3PCLK1|RCC_CLOCKTYPE_D1PCLK1;
 RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
 RCC_ClkInitStruct.SYSCLKDivider = RCC_SYSCLK_DIV1;
 RCC_ClkInitStruct.AHBCLKDivider = RCC_HCLK_DIV2;
 RCC_ClkInitStruct.APB3CLKDivider = RCC_APB3_DIV1;
 RCC_ClkInitStruct.APB1CLKDivider = RCC_APB1_DIV1;
 RCC_ClkInitStruct.APB2CLKDivider = RCC_APB2_DIV1;
 RCC_ClkInitStruct.APB4CLKDivider = RCC_APB4_DIV1;
 if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK){
  Error_Handler();
 }
}

static void MX_USART3_UART_Init(void){
 UartHandle.Instance = USART3;
 UartHandle.Init.BaudRate = 115200;
 UartHandle.Init.WordLength = UART_WORDLENGTH_8B;
 UartHandle.Init.StopBits = UART_STOPBITS_1;
 UartHandle.Init.Parity = UART_PARITY_NONE;
 UartHandle.Init.Mode = UART_MODE_TX_RX;
 UartHandle.Init.HwFlowCtl = UART_HWCONTROL_NONE;
 UartHandle.Init.OverSampling = UART_OVERSAMPLING_16;
 UartHandle.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
 UartHandle.Init.ClockPrescaler = UART_PRESCALER_DIV1;
 UartHandle.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
 if(HAL_UART_Init(&UartHandle) != HAL_OK){
  Error_Handler();
 }
 if(HAL_UARTEx_SetTxFifoThreshold(&UartHandle, UART_TXFIFO_THRESHOLD_1_8)
   != HAL_OK){
  Error_Handler();
 }
 if(HAL_UARTEx_SetRxFifoThreshold(&UartHandle, UART_RXFIFO_THRESHOLD_1_8)
   != HAL_OK){
  Error_Handler();
 }
 if(HAL_UARTEx_DisableFifoMode(&UartHandle) != HAL_OK){
  Error_Handler();
 }
}

static void MX_GPIO_Init(void){
 GPIO_InitTypeDef GPIO_InitStruct = {0};
 __HAL_RCC_GPIOA_CLK_ENABLE();
 __HAL_RCC_GPIOB_CLK_ENABLE();
 __HAL_RCC_GPIOD_CLK_ENABLE();
 __HAL_RCC_GPIOE_CLK_ENABLE();
 __HAL_RCC_GPIOF_CLK_ENABLE();
 __HAL_RCC_GPIOG_CLK_ENABLE();

 /* a[10:0] signal
  * a[0]  <- PG1
  * a[1]  <- PF9
  * a[2]  <- PF7
  * a[3]  <- PF0
  * a[4]  <- PF1
  * a[5]  <- PF2
  * a[6]  <- PE5
  * a[7]  <- PE6
  * a[8]  <- PE3
  * a[9]  <- PF8
  * a[10] <- PA0
  */
 /* reset state */
 HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, GPIO_PIN_RESET);
 HAL_GPIO_WritePin(GPIOE, GPIO_PIN_3|GPIO_PIN_5|GPIO_PIN_6,
   GPIO_PIN_RESET);
 HAL_GPIO_WritePin(GPIOF, GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|
   GPIO_PIN_7|GPIO_PIN_8|GPIO_PIN_9, GPIO_PIN_RESET);
 HAL_GPIO_WritePin(GPIOG, GPIO_PIN_1, GPIO_PIN_RESET);
 /* output push pull mode, no pull-up or pull-down */
 GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
 GPIO_InitStruct.Pull = GPIO_NOPULL;
 GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
 /* PA0 */
 GPIO_InitStruct.Pin = GPIO_PIN_0;
 HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
 /* PE3, PE5, PE6  */
 GPIO_InitStruct.Pin = GPIO_PIN_3|GPIO_PIN_5|GPIO_PIN_6;
 HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);
 /* PF0, PF1, PF2, PF7, PF8, PF9  */
 GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|
   GPIO_PIN_7|GPIO_PIN_8|GPIO_PIN_9;
 HAL_GPIO_Init(GPIOF, &GPIO_InitStruct);
 /* PG1 */
 GPIO_InitStruct.Pin = GPIO_PIN_1;
 HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

 /* io[7:0] signal
  * io[0] <-> PG0
  * io[1] <-> PD1
  * io[2] <-> PD0
  * io[3] <-> PB11
  * io[4] <-> PB10
  * io[5] <-> PE15
  * io[6] <-> PE12
  * io[7] <-> PE2
  */
 /* input mode */
 GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
 GPIO_InitStruct.Pull = GPIO_NOPULL;
 GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
 /* PB10, PB11 */
 GPIO_InitStruct.Pin = GPIO_PIN_10|GPIO_PIN_11;
 HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
 /* PD0, PD1 */
 GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1;
 HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);
 /* PE2, PE12, PE15 */
 GPIO_InitStruct.Pin = GPIO_PIN_2|GPIO_PIN_12|GPIO_PIN_15;
 HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);
 /* PG0 */
 GPIO_InitStruct.Pin = GPIO_PIN_0;
 HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

 /* OE# signal
  * OE#  <- PE0
  */
 /* set state */
 HAL_GPIO_WritePin(GPIOG, GPIO_PIN_1, GPIO_PIN_SET);
 /* output push pull mode, no pull-up or pull-down */
 GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
 GPIO_InitStruct.Pull = GPIO_NOPULL;
 GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
 /* PE0 */
 GPIO_InitStruct.Pin = GPIO_PIN_0;
 HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

 /* R/C# signal
  * R/C# <- PB0
  */
 /* set state */
 HAL_GPIO_WritePin(GPIOG, GPIO_PIN_1, GPIO_PIN_SET);
 /* output push pull mode, no pull-up or pull-down */
 GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
 GPIO_InitStruct.Pull = GPIO_NOPULL;
 GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
 /* PB0 */
 GPIO_InitStruct.Pin = GPIO_PIN_0;
 HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
}

void Error_Handler(void){
 __disable_irq();
 while(1){

 }
}
