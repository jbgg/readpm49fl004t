
#ifndef __SIGNALS_H__
#define __SIGNALS_H__

#include <stdint.h>
#include "stm32h7xx_hal.h"

#define OE_PORT GPIOE
#define OE_PIN GPIO_PIN_0
#define RC_PORT GPIOB
#define RC_PIN GPIO_PIN_0
#define A0_PORT GPIOG
#define A0_PIN GPIO_PIN_1
#define A1_PORT GPIOF
#define A1_PIN GPIO_PIN_9
#define A2_PORT GPIOF
#define A2_PIN GPIO_PIN_7
#define A3_PORT GPIOF
#define A3_PIN GPIO_PIN_0
#define A4_PORT GPIOF
#define A4_PIN GPIO_PIN_1
#define A5_PORT GPIOF
#define A5_PIN GPIO_PIN_2
#define A6_PORT GPIOE
#define A6_PIN GPIO_PIN_5
#define A7_PORT GPIOE
#define A7_PIN GPIO_PIN_6
#define A8_PORT GPIOE
#define A8_PIN GPIO_PIN_3
#define A9_PORT GPIOF
#define A9_PIN GPIO_PIN_8
#define A10_PORT GPIOA
#define A10_PIN GPIO_PIN_0

#define IO0_PORT GPIOG
#define IO0_PIN GPIO_PIN_0
#define IO1_PORT GPIOD
#define IO1_PIN GPIO_PIN_1
#define IO2_PORT GPIOD
#define IO2_PIN GPIO_PIN_0
#define IO3_PORT GPIOB
#define IO3_PIN GPIO_PIN_11
#define IO4_PORT GPIOB
#define IO4_PIN GPIO_PIN_10
#define IO5_PORT GPIOE
#define IO5_PIN GPIO_PIN_15
#define IO6_PORT GPIOE
#define IO6_PIN GPIO_PIN_12
#define IO7_PORT GPIOE
#define IO7_PIN GPIO_PIN_2

static inline void oe_w(uint8_t v){
 HAL_GPIO_WritePin(OE_PORT, OE_PIN, v ? GPIO_PIN_SET : GPIO_PIN_RESET);
}
static inline void rc_w(uint8_t v){
 HAL_GPIO_WritePin(RC_PORT, RC_PIN, v ? GPIO_PIN_SET : GPIO_PIN_RESET);
}
static inline void a_w(uint16_t v){
 HAL_GPIO_WritePin(A0_PORT, A0_PIN,
   v & (1<<0) ? GPIO_PIN_SET : GPIO_PIN_RESET);
 HAL_GPIO_WritePin(A1_PORT, A1_PIN,
   v & (1<<1) ? GPIO_PIN_SET : GPIO_PIN_RESET);
 HAL_GPIO_WritePin(A2_PORT, A2_PIN,
   v & (1<<2) ? GPIO_PIN_SET : GPIO_PIN_RESET);
 HAL_GPIO_WritePin(A3_PORT, A3_PIN,
   v & (1<<3) ? GPIO_PIN_SET : GPIO_PIN_RESET);
 HAL_GPIO_WritePin(A4_PORT, A4_PIN,
   v & (1<<4) ? GPIO_PIN_SET : GPIO_PIN_RESET);
 HAL_GPIO_WritePin(A5_PORT, A5_PIN,
   v & (1<<5) ? GPIO_PIN_SET : GPIO_PIN_RESET);
 HAL_GPIO_WritePin(A6_PORT, A6_PIN,
   v & (1<<6) ? GPIO_PIN_SET : GPIO_PIN_RESET);
 HAL_GPIO_WritePin(A7_PORT, A7_PIN,
   v & (1<<7) ? GPIO_PIN_SET : GPIO_PIN_RESET);
 HAL_GPIO_WritePin(A8_PORT, A8_PIN,
   v & (1<<8) ? GPIO_PIN_SET : GPIO_PIN_RESET);
 HAL_GPIO_WritePin(A9_PORT, A9_PIN,
   v & (1<<9) ? GPIO_PIN_SET : GPIO_PIN_RESET);
 HAL_GPIO_WritePin(A10_PORT, A10_PIN,
   v & (1<<10) ? GPIO_PIN_SET : GPIO_PIN_RESET);
}

static inline uint8_t io_r(){
 uint8_t ret;
 ret = 0;
 ret |= HAL_GPIO_ReadPin(IO0_PORT, IO0_PIN) ? (1 << 0) : 0;
 ret |= HAL_GPIO_ReadPin(IO1_PORT, IO1_PIN) ? (1 << 1) : 0;
 ret |= HAL_GPIO_ReadPin(IO2_PORT, IO2_PIN) ? (1 << 2) : 0;
 ret |= HAL_GPIO_ReadPin(IO3_PORT, IO3_PIN) ? (1 << 3) : 0;
 ret |= HAL_GPIO_ReadPin(IO4_PORT, IO4_PIN) ? (1 << 4) : 0;
 ret |= HAL_GPIO_ReadPin(IO5_PORT, IO5_PIN) ? (1 << 5) : 0;
 ret |= HAL_GPIO_ReadPin(IO6_PORT, IO6_PIN) ? (1 << 6) : 0;
 ret |= HAL_GPIO_ReadPin(IO7_PORT, IO7_PIN) ? (1 << 7) : 0;
 return ret;
}

#endif /* __SIGNALS_H__ */
