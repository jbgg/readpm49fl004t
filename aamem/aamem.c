
#include "io.h"
#include "signals.h"
#include "xmodem.h"
#include "aamem.h"

int ret;

int aamem_init(){
 ret = 0;
 a_w(0);
 oe_w(1);
 rc_w(1);
 return 0;
}

int aamem_read(uint8_t *o, uint32_t addr, uint16_t s){
 int i;
 /* init */
 oe_w(1);
 rc_w(1);
 /* enable data */
 oe_w(0);
 for(i=0;i<s;i++){
  /* write row addr */
  a_w(addr & 0x7ff);
  rc_w(0);
  /* write column addr */
  a_w((addr >> 11) & 0x1ff);
  rc_w(1);
  /* read data */
  o[i] = io_r();
  addr++;
 }
 /* disable data */
 oe_w(1);
 return 0;
}

int sx(uint32_t addr){
 int i;
 char d[1024];
 ret = 1;
 if(xmodem_init()){
  return 1;
 }
 /* 512K */
 for(i=0;i<512;i++){
  if(aamem_read(d, addr, 1024)){
   return 1;
  }
  if(xmodem_send(d, 1024)){
   return 1;
  }
  addr += 1024;
 }
 if(xmodem_end()){
  return 1;
 }else{
  ret = 0;
 }
 return 0;
}

int cmd_sx(char *args){
 if(sx(0x000000)){
  return 1;
 }
 return 0;
}

int cmd_init(char *args){
 aamem_init();
 return 0;
}
int cmd_read(char *args){
 uint8_t o[16];
 uint32_t addr;
 uint16_t s;
 int i;
 addr = 0x7fff0;
 s = 16;
 if(aamem_read(o, addr, s)){
  io_printf("error\r\n");
  return 1;
 }
 io_printf("%05x: ", addr);
 for(i=0;i<16;i++){
  io_printf("%02x ", o[i] & 0xff);
 }
 io_printf("\r\n");
 return 0;
}

int cmd_ret(char *args){
 io_printf("%d\r\n", ret);
 return 0;
}
