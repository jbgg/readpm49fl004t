
#ifndef __AAMEM_H__
#define __AAMEM_H__

#include <stdint.h>

int aamem_init();
int aamem_read(uint8_t *o, uint32_t addr, uint16_t s);

#endif /* __AAMEM_H__ */
